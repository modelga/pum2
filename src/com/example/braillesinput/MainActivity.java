package com.example.braillesinput;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity implements OnInitListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Intent checkIntent = new Intent();
		checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
		startActivityForResult(checkIntent, 1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		TextToSpeech textToSpeech = new TextToSpeech(this, null);
		textToSpeech.speak("Try", 0, null);
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private TextToSpeech mTts;

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1) {
			if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
				mTts = new TextToSpeech(this, this);
			} else {
				Intent installIntent = new Intent();
				installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
				startActivity(installIntent);
			}
		}
	}

	@Override
	public void onInit(int status) {
		mTts.setLanguage(new Locale("pl_PL"));
		Locale language = mTts.getLanguage();
		for (Locale checkLocale : Locale.getAvailableLocales()) {
			if (mTts.isLanguageAvailable(checkLocale) == TextToSpeech.LANG_AVAILABLE) {
				TextView t = (TextView) findViewById(R.id.hello);
				t.append(checkLocale.toString() + "\r\n");

			}
		}
		mTts.setSpeechRate((float) 0.5);
	}
}
